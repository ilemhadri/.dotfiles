""""" USER SETTINGS
set encoding=utf-8
set incsearch
set scrolloff=5
:let mapleader = ","
" display line number
set number
set relativenumber
set incsearch


"TABS 
set splitbelow
set splitright

nnoremap <leader>j <C-W><C-J> 
nnoremap <leader>k <C-W><C-K> 
nnoremap <leader>h <C-W><C-H> 
nnoremap <leader>l <C-W><C-L> 


" Set up Vundlel
set nocompatible	" Use Vim defaults (much better!)
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'tpope/vim-commentary'
" Plugin 'vim-syntastic/syntastic'
call vundle#end()
" to ignore plugin indent changes, instead use:
"filetype plugin on
""
" Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line
"
filetype plugin indent on
autocmd Filetype python set tabstop=4 expandtab softtabstop=4 textwidth=79 fileformat=unix
autocmd BufNewFile,BufRead *.js,*.html,*.css set tabstop=2 softtabstop=2 shiftwidth=2
if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
   set fileencodings=ucs-bom,utf-8,latin1
endif

colo desert

"Easymotion settings
let g:EasyMotion_smartcase =1
map <SPACE> <Plug>(easymotion-s2)

"""" DEFAULT SETTINGS
set bs=indent,eol,start		" allow backspacing over everything in insert mode
"set ai			" always set autoindenting on
"set backup		" keep a backup file
set viminfo='20,\"50,:20,%,n~/.viminfo	" read/write a .viminfo file, don't store more
			" than 50 lines of registers
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time

" http://vim.wikia.com/wiki/Restore_cursor_to_file_position_in_previous_editing_session
function! ResCur()
	  if line("'\"") <= line("$")
		      normal! g`"
		          return 1
			    endif
		    endfunction

		    augroup resCur
			      autocmd!
			        autocmd BufWinEnter * call ResCur()
			augroup END

if has("cscope") && filereadable("/usr/bin/cscope")
   set csprg=/usr/bin/cscope
   set csto=0
   set cst
   set nocsverb
   " add any database in current directory
   if filereadable("cscope.out")
      cs add $PWD/cscope.out
   " else add database pointed to by environment
   elseif $CSCOPE_DB != ""
      cs add $CSCOPE_DB
   endif
   set csverb
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif


if &term=="xterm"
     set t_Co=8
     set t_Sb=[4%dm
     set t_Sf=[3%dm
endif

" Don't wake up system with blinking cursor:
" http://www.linuxpowertop.org/known.php
let &guicursor = &guicursor . ",a:blinkon0" 

" Limit 79 characters per nine
set tw=79
