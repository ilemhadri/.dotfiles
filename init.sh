#!/bin/bash
line="============================================"
clear
text="if [ -f "$PWD/.bashrc" ] ; then
  source "$PWD/.bashrc"
fi"
echo "           lemisma's dotfiles"
echo "$line"
echo -e "This script downloads and installs lemisma's dotfiles. \nIt also overwrites .bash_profile in your HOME directory. Git required for the download."
while true; do
	read -p "Do you wish to proceed? (yes/no) " choice
	case $choice in
		no ) 
	    	    	exit 1;;
		yes )
			echo "alias vim='vim -u $PWD/.vimrc'" >> $PWD/.bashrc;
			echo "tmux source-file $PWD/.tmux.conf" >> $PWD/.bashrc;
			echo "$text" > ~/.bash_profile;
			git clone https://github.com/VundleVim/Vundle.Vim.git ~/.vim/bundle/Vundle.vim;
			source ~/.bash_profile;
			vim --cmd "source $PWD/.vimrc" --cmd "PluginInstall" --cmd "q" ;
			# echo "To finish this setup, open vim and type the command \" :PluginInstall \" . Note: you may first need to source the .vimrc";
			exit 1;;
    	   	* )
			echo -e "Please type yes or no \n";
	esac	
done
