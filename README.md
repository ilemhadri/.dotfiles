# **lemisma's dotfiles**
My configuration files for Linux machines. I use this whenever I have to log to a new machine and want to use my personal bash, vim and tmux settings.

#
## Usage
This script overwrites the ~/.bash_profile to source the downloaded copy of my .bashrc. It also installs Vundle to your ~/.vim/bundle directory and various vim plugins. Finally, it changes some of the key mappings of tmux.

# Setup
```bash
# Get a local copy of the folder
git clone https://ilemhadri@bitbucket.org/ilemhadri/.dotfiles.git

# Install everything
./init.sh

```
